{-# LANGUAGE ScopedTypeVariables #-}

module Main (main) where

import System.Environment (getArgs)

import qualified Data.ByteString.Char8 as BSC (pack, unpack)
import qualified Data.Binary as BIN (decode)
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS (replicate, concat,
                                        fromStrict,
                                        readFile, writeFile,
                                        take, drop, split,
                                        takeEnd, dropEnd,
                                        dropWhile)
import Text.Regex.PCRE ((=~))



main :: IO ()
main = do
  args <- getArgs
  if length args == 1 then
    extractFile (head args)
    else
    error "invalid number of arguments, need one"
      

extractFile :: String -> IO ()
extractFile fileName = do
  bs <- BS.readFile fileName
  let (bin, txt) = separateCacheFile bs
      contentType' = lookup "content-type" $ parseInfo txt
      ext' = maybe (error "no content-type") parseExtension contentType'
    in
    case ext' of
      Just ext -> BS.writeFile (fileName ++ "bin" ++ ext) bin
      Nothing -> error "invalid Extension"


{--
This functions takes the last 4 bytes of the file to know the length of the
binary part of the cached data. After that it separates between the binary data
and its meta info
--}
separateCacheFile :: ByteString -> (ByteString, ByteString)
separateCacheFile bs =
  let intS = BS.fromStrict $ BS.concat [BS.replicate 4 0, BS.takeEnd 4 bs]
      binSize = BIN.decode intS :: Int

      bin = BS.take binSize bs
      txt = BS.dropWhile (== 10) $ BS.drop binSize bs
    in
    (bin, txt)


{--
This functions parses the meta info part to an assoc list of
keys and values correspodent to each file property
--}
parseInfo :: ByteString -> [(String, String)]
parseInfo infoText =
  let txtLines = drop 1 $ BS.split 10 infoText
  in
    foldr (\ x ls -> (parseLine x) : ls) [] txtLines

  where
    parseLine :: ByteString -> (String, String)
    parseLine line = 
      let key_regex = BSC.pack "(\\w|_|-)+:"
          value_regex = BSC.pack ": .*"
      in
        (BSC.unpack $ BS.dropEnd 1 $ line =~ key_regex,
         BSC.unpack $ BS.drop 2 $ BS.dropEnd 1 $ line =~ value_regex)


parseExtension :: String -> Maybe String
parseExtension ctype
  | (ctype =~ "webm") /= "" = Just ".webm"
  | (ctype =~ "gif") /= "" = Just ".gif"
  | (ctype =~ "jpeg") /= "" = Just ".jpeg"
  | (ctype =~ "jpg") /= "" = Just ".jpg"
  | (ctype =~ "png") /= "" = Just ".png"
  | (ctype =~ "css") /= "" = Just ".css"
  | (ctype =~ "mp4") /= "" = Just ".mp4"
  | (ctype =~ "javascript") /= "" = Just ".js"
  | otherwise = Nothing



